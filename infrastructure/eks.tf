module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_version = var.cluster_version
  cluster_name    = var.eks_cluster_name
  subnets         = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = var.worker_name
      instance_type                 = var.worker_type
      asg_desired_capacity          = 3
      additional_security_group_ids = [aws_security_group.worker_node_sg.id]
    }
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}