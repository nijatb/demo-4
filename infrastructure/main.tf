terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.20.0"
    }
  }
  backend "s3" {
    bucket = "terraform-state-bucket01"
    key    = "petclinic/terraform.tfstate"
    region = "us-west-1"
  }
}
provider "aws" {
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
  region     = "us-west-1"
}