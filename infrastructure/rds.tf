resource "aws_db_instance" "petclinic_db" {
  allocated_storage = 10
  #   apply_immediately    = true
  publicly_accessible    = false
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "8.0.20"
  instance_class         = "db.t2.micro"
  identifier             = var.petclinic_DB_NAME
  name                   = var.petclinic_DB_NAME
  username               = var.petclinic_DB_USERNAME
  password               = var.petclinic_DB_PASS
  skip_final_snapshot    = true
  db_subnet_group_name   = "petclinic_db_subnet_group"
  vpc_security_group_ids = [aws_security_group.rds-sg.id]
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_db"
  }
  depends_on = [aws_db_subnet_group.petclinic_db_subnet_group]
}

resource "aws_db_subnet_group" "petclinic_db_subnet_group" {
  name       = "petclinic_db_subnet_group"
  subnet_ids = module.vpc.private_subnets
  depends_on = [module.vpc.private_subnets, aws_security_group.rds-sg]

  tags = {
    Name  = "petclinic_db_subnet_group"
    Owner = "NicatB"
  }
}

resource "null_resource" "rds_identifier_gathering" {
  provisioner "local-exec" {
    command     = <<-EOT
      aws rds describe-db-instances  --db-instance-identifier ${var.petclinic_DB_NAME} --region us-west-1 --query "(DBInstances[*].Endpoint.Address)" --output text > db_endpoint.txt
      sed -i -e "s/.*SPRING_DATASOURCE_URL.*/  SPRING_DATASOURCE_URL: jdbc:mysql:\/\/$(cat db_endpoint.txt):3306\/${var.petclinic_DB_NAME}/g" ../app_deploy/secrets.yml
  EOT

  }
  depends_on = [aws_db_instance.petclinic_db]
}

output "rds_address" {
  value = aws_db_instance.petclinic_db.address
}

