variable "region" {
  default = "us-west-1"
}

############################################
# VPC Variables
############################################
variable "cidr_block" {
  description = "CIDR Block for the VPC"
}

variable "private_subnets" {
  description = "VPC subnets for k8s cluster"
}

variable "public_subnets" {
  description = "VPC subnets for load balancer"
}
variable "availability_zones" {
  default = ["usw1-az1", "usw1-az3"]
  type    = list(string)
}

variable "cluster_version" {
  default = ""
}

variable "worker_name" {
  description = "Name of the workers"
}

variable "worker_type" {
  description = "Type of the workers"
}

###############################################
# Variables for the AWS auth
###############################################

// Will be set from build env
variable "AWS_ACCESS_KEY_ID" {
  description = "AWS Access Key"
}

// Will be set from build env
variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS Secret Key"
}
variable "eks_cluster_name" {
  description = "EKS Cluster Name"
}
####################################################
# DB Variables
####################################################
variable "petclinic_DB_NAME" {}
variable "petclinic_DB_USERNAME" {}
variable "petclinic_DB_PASS" {}