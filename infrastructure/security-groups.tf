resource "aws_security_group" "worker_node_sg" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [var.cidr_block]
  }
}

resource "aws_security_group" "rds-sg" {
  name        = "rds-sg"
  description = "RDS Security Group rules"
  vpc_id      = module.vpc.vpc_id
  ingress {
    description = "RDS Inbound rules"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"

    cidr_blocks = [var.cidr_block]
  }
  egress {
    description = "RDS Outbound rules"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}